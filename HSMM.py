from hsmmlearn.hsmm import GaussianHSMM
from hsmmlearn.hsmm import HSMMModel
from hsmmlearn.emissions import AbstractEmissions, MultinomialEmissions
from scipy.stats import laplace
import numpy as np

# Note: this is almost identical to the GaussianEmissions class,
# the only difference being that we replaced the Gaussian RV (norm)
# with a Laplacian RV (laplace).
class LaplaceEmissions(AbstractEmissions):

    # Note: this property is a hack, and will become unnecessary soon!
    dtype = np.float64
        
    def __init__(self, means, scales):
        self.means = means
        self.scales = scales

    def likelihood(self, obs):
        obs = np.squeeze(obs)
        # TODO: build in some check for the shape of the likelihoods, otherwise
        # this will silently fail and give the wrong results.
        return laplace.pdf(obs,
                           loc=self.means[:, np.newaxis],
                           scale=self.scales[:, np.newaxis])

    def sample_for_state(self, state, size=None):
        return laplace.rvs(self.means[state], self.scales[state], size)

def hsmm_get_transition_matrix(test_sub, sub_labels, samples):
    tm_cnt = np.zeros((2, 2), dtype=float)
    for temp_sub in samples:
        if temp_sub == test_sub:
            continue

        sub_seq = list(sub_labels[sub_labels[0] == temp_sub][1])
        for idx in range(len(sub_seq) - 1):
            if sub_seq[idx] == sub_seq[idx+1]:
                continue
            tm_cnt[sub_seq[idx], sub_seq[idx+1]] += 1
    tm = np.zeros((2, 2), dtype=float)
    for i in range(2):
        for j in range(2):
            tm[i, j] = tm_cnt[i, j] / np.sum(tm_cnt[i])
    return tm


def hsmm_get_duration(test_sub, sub_labels, samples):
    dr_cnt = np.zeros((2, 1000), dtype=float)
    for temp_sub in samples:
        if temp_sub == test_sub:
            continue

        sub_seq = list(sub_labels[sub_labels.iloc[:,0] == temp_sub].iloc[:,1])
        idx = 0
        pre_state = 0
        cnt = 0
        while idx < len(sub_seq):
            if sub_seq[idx] == pre_state:
                cnt += 1
            else: 
                if cnt != 0:
                    dr_cnt[pre_state, cnt-1] += 1
                pre_state = sub_seq[idx]
                cnt = 1
            idx += 1
        if cnt != 0:
            dr_cnt[pre_state, cnt-1] += 1
    dr = np.zeros((2, 1000), dtype=float)
    for i in range(2):
        dr[i] = dr_cnt[i] / np.sum(dr_cnt[i])
    return dr

def new_hsmm_get_duration(test_sub, sub_labels, samples):
    test_len = 600
    dr_cnt = np.ones((2, test_len), dtype=float)
    for temp_sub in samples:
        if temp_sub == test_sub:
            continue

        sub_seq = list(sub_labels[sub_labels.iloc[:,0] == temp_sub].iloc[:,1])
        idx = 0
        pre_state = 0
        cnt = 0
        while idx < len(sub_seq):
            if sub_seq[idx] == pre_state:
                cnt += 1
            else: 
                if cnt != 0:
                    if cnt > test_len:
                        cnt = test_len
                    dr_cnt[pre_state, cnt-1] += 100
                pre_state = sub_seq[idx]
                cnt = 1
            idx += 1
        if cnt != 0:
            if cnt > test_len:
                cnt = test_len
            dr_cnt[pre_state, cnt-1] += 100
    dr = np.zeros((2, test_len), dtype=float)
    for i in range(2):
        dr[i] = dr_cnt[i] / np.sum(dr_cnt[i])
    return dr

def hsmm_get_init_probability(samples, test_sub, subjects, labels):
    ini_prob = []
    cnt0 = 0
    cnt1 = 0
    for test_sub in samples:
        test_lb = []
        for idx, sub in enumerate(subjects):
            if sub == test_sub:
                test_lb.append(labels[idx])
        ini_prob.append(test_lb[0])
        if test_lb[0] == 0:
            cnt0 += 1
        else:
            cnt1 += 1
    return np.array([cnt0 / 49, cnt1 / 49])
