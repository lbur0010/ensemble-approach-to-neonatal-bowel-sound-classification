import matplotlib.pyplot as plt
import sys
import os

def plot_history(history, test_sub, save_path):
    #  "Accuracy"
    plt.figure()
    plt.plot(history.history['accuracy'])
    plt.plot(history.history['val_accuracy'])
    plt.plot(history.history['loss'])
    plt.plot(history.history['val_loss'])
    plt.ylabel('Accuracy/Loss Curves')
    plt.xlabel('Epoch')
    plt.legend(['Train Acc', 'Validation Acc', 'Train Loss', 'Validation Loss'], loc='right')
    plt.savefig(f"{save_path}/{str(test_sub)}.png")

# Disable
def blockPrint():
    sys.stdout = open(os.devnull, 'w')

# Restore
def enablePrint(stdout):
    sys.stdout = stdout