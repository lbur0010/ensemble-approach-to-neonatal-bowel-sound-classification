from Dataset import Dataset
from Model import NeoNatalModel
from Run import Run
import pickle
import numpy as np

np.random.seed(42)

path = 'G:/Neonatal Codebase/raw_data'
data_path = 'G:/Neonatal Codebase/processed_data/'
model_path = 'G:/Neonatal Codebase/models'
path_1d = '1d_models'
path_2d = '2d_models'
ensemble_path = 'ensemble_models/no_under'
MLP_path = 'MLP_models/no_under'

dataset = Dataset(path, data_path, create_data=False) # If create_data is false, loads previously processed data
model = NeoNatalModel(model_path, path_1d, path_2d, ensemble_path) # Creates ensemble model based on previously trained 1D and 2D models
run = Run(dataset)

run.train_test_ensemble(model, retrain=False, under_sample=False, patience=2) # When retrain is false, will only train a model if it cannot find pre-trained weights
run.train_test_MLP(f"{model_path}/{MLP_path}", retrain=False, under_sample=False) # When retrain is false, will only train a model if it cannot find pre-trained weights

run.refine_classification(refine_type = "ensemble", clf = None) # Performs HSMM refinement, then saves the best S predictions for a later function
run.load_refinement(refine_type="ensemble") # Loads the best S predictions

# A dummy instance of the MLP model must be loaded so that the code knows how many classes the model outputs
with open(f"{model_path}/{MLP_path}/{str(dataset.subjects[0])}.pt", "rb") as fp:
    clf = pickle.load(fp)

# run.max_voting(clf)

run.refine_classification(refine_type = "MLP", clf = clf) # Performs HSMM refinement, then saves the best S predictions for a later function
run.load_refinement(refine_type="MLP") # Loads the best S predictions 
run.averaged_HSMM_refinement()

