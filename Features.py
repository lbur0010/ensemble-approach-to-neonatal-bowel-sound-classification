from python_speech_features import mfcc, fbank, logfbank, ssc
from pydub.silence import detect_silence
from soundfile import write
from pydub import AudioSegment
import numpy as np
import scipy as sp
import pandas as pd
import pywt
import math
import warnings
warnings.filterwarnings('ignore')

'''
    Global parameters tunning
'''

numcep=24              # the number of cepstrum to return, applied in get_mfcc only
winlen = 0.025         # the length of the analysis window in seconds. Default is 0.025s (25 milliseconds)
winstep = 0.01         # the step between successive windows in seconds. Default is 0.01s (10 milliseconds)
nfilt = 26             # the number of filters in the filterbank
nfft = 512             # the FFT size, used to determine the width of frequency bin
lowfreq = 0            # lowest band edge of mel filters
highfreq = None        # highest band edge of mel filters. In Hz, default is samplerate/2
        
def get_mfcc(wave, sr, numcep=numcep, winlen=winlen, winstep=winstep, nfilt=nfilt, nfft=nfft, lowfreq=lowfreq, highfreq=highfreq):
    '''
        Aim: Compute MFCC features from an audio signal.
        Return: A numpy array of size (NUMFRAMES by numcep) containing features. Each row holds 1 feature vector.
    '''
    return mfcc(
        signal=wave, 
        samplerate=sr, 
        winlen=winlen, 
        winstep=winstep, 
        numcep=24,
        nfilt=nfilt,           
        nfft=nfft, 
        lowfreq=lowfreq, 
        highfreq=highfreq, 
    )

def get_fbank_en(wave, sr, winlen=winlen, winstep=winstep, nfilt=nfilt, nfft=nfft, lowfreq=lowfreq, highfreq=highfreq):
    '''
        Aim: Compute Mel-filterbank energy features from an audio signal. 
        Return: 
            A tuple of 2 values. 
            The first is a numpy array of size (NUMFRAMES by nfilt) containing features. Each row contins sub-band Energies of each Frame.   
            The second return value is the energy in each frame (total energy, unwindowed). Frame Energy
    '''
    return fbank(
        signal=wave, 
        samplerate=sr, 
        winlen=winlen, 
        winstep=winstep, 
        nfilt=nfilt,           
        nfft=nfft, 
        lowfreq=lowfreq, 
        highfreq=highfreq,
         )[0]

def get_log_fbank_en(wave, sr, winlen=winlen, winstep=winstep, nfilt=nfilt, nfft=nfft, lowfreq=lowfreq, highfreq=highfreq):
    '''
        Aim: Compute log Mel-filterbank energy features from an audio signal. 
        Return: 
            A numpy array of size (NUMFRAMES by nfilt) containing features. Each row contins log subband Energyies of each Frame
    '''
    return logfbank(
        signal=wave, 
        samplerate=sr, 
        winlen=winlen, 
        winstep=winstep, 
        nfilt=nfilt,           
        nfft=nfft, 
        lowfreq=lowfreq, 
        highfreq=highfreq,
         )


def get_ssc(wave, sr, winlen=winlen, winstep=winstep, nfilt=nfilt, nfft=nfft, lowfreq=lowfreq, highfreq=highfreq):
    '''
        Aim: Compute Spectral Subband Centroid features from an audio signal.
        Return: A numpy array of size (NUMFRAMES by nfilt) containing features. Each row holds 1 feature vector.
    '''
    return ssc(
        signal=wave, 
        samplerate=sr, 
        winlen=winlen, 
        winstep=winstep, 
        nfilt=nfilt,           
        nfft=nfft, 
        lowfreq=lowfreq, 
        highfreq=highfreq,
         )

def hz2mel(hz):
    """Convert a value in Hertz to Mels
    :param hz: a value in Hz. This can also be a numpy array, conversion proceeds element-wise.
    :returns: a value in Mels. If an array was passed in, an identical sized array is returned.
    """
    return 2595 * np.log10(1+hz/700.)

def mel2hz(mel):
    """Convert a value in Mels to Hertz
    :param mel: a value in Mels. This can also be a numpy array, conversion proceeds element-wise.
    :returns: a value in Hertz. If an array was passed in, an identical sized array is returned.
    """
    return 700*(10**(mel/2595.0)-1)

def get_fre_range(name, samplerate=4000, winlen=winlen, winstep=winstep, nfilt=nfilt, nfft=nfft, lowfreq=lowfreq, highfreq=highfreq):
    highfreq = samplerate/2
    lowmel = hz2mel(lowfreq)
    highmel = hz2mel(highfreq)
    melpoints = np.linspace(lowmel,highmel,nfilt+2)
    frepoints = mel2hz(melpoints)
    return [name + '_' + str(round(frepoints[i])) + 'hz - ' + str(round(frepoints[i+2])) + 'hz' for i in range(len(frepoints)-2)]

def get_fre_range1(samplerate=4000, winlen=winlen, winstep=winstep, nfilt=nfilt, nfft=nfft, lowfreq=lowfreq, highfreq=highfreq):
    highfreq = samplerate/2
    lowmel = hz2mel(lowfreq)
    highmel = hz2mel(highfreq)
    melpoints = np.linspace(lowmel,highmel,nfilt+2)
    frepoints = mel2hz(melpoints)
    return ['[' + str(round(frepoints[i])) + ' - ' + str(round(frepoints[i+2])) + 'Hz]' for i in range(len(frepoints)-2)]

def jit_shim(data, sound):
    # Fourier Transform from data 
    fourier_transform = np.fft.rfft(data)

    # Frequencies
    frequencies = np.abs(fourier_transform)

    # Maximum Pitch
    max_pitch = np.argmax(frequencies)

    arr = [117]*len(frequencies)

    sum_freq = 0

    for i in range(0,len(frequencies)):
        if int(frequencies[i])>0 and int(frequencies[i])<int(max_pitch):
            arr[i] = int(frequencies[i])
            sum_freq=sum_freq + i

    #SILENCES
    pauses=detect_silence(sound, min_silence_len=20, silence_thresh=-(round(abs(sound.dBFS))+100), seek_step=1)

    #FINDING DURATIONS
    sumd=0
    ls=[]
    for start_i, end_i in pauses:
            dur=int(end_i)-int(start_i)
            ls.append(dur)
    breaks=[]

    #VOICE BREAKS
    for i in ls:
        if i >900:
            breaks.append(i)

    #SEPRATING BREAKES AND PAUSES
    ls=list(set(ls)-set(breaks))

    #PUASES DURATION
    for i in ls:
        sumd=sumd+i

    #FINDING PEAK
    peak=sp.signal.find_peaks(data,rel_height=0.5)

    sums=0
    #JITTER, SHIMMER, JITTERRAP 
    for i in range(1,len(peak[0])-1):
        sums=sums+abs(20*math.log10(peak[0][i+1]/peak[0][i]))

    #SHIMMER
    shimmer=sums/(len(peak[0])-1)
    peakf=abs(np.fft.fft(peak[0]))
    sumps=0
    for i in range(1,len(peakf)-1):
        sumps=sumps+(peakf[i+1]**-1)-(peakf[i]**-1)

    #JITTER
    jitter=sumps/(len(peakf)-1)
    sortedp=np.sort(peak[0])
    sortedf=abs(np.fft.fft(sortedp))
    dif=abs(sortedp[11]-sortedp[15])
    suh=0
    avgabsdiff=(dif)/4
    avgneigh1=(abs(sortedp[6]-sortedp[10]))
    avgneigh2=abs(sortedp[17]-sortedp[22])
    avg=(dif+avgneigh1+avgneigh2)/3

    for i in range(11,16):
        suh=suh+abs(sortedf[i]**-1)
    period=suh/5

    #JITTERRAP
    jitterrap=(avgabsdiff+avg)/period

    return jitter, jitterrap, shimmer

def get_features(seg, sr):
    fidx = []
    fvalue = [] 
    
    write('seg.wav', seg, sr)
    sound = AudioSegment.from_wav("seg.wav")

    # jitter and shimmer
    fvalue += jit_shim(seg, sound)
    fidx += ['localJitter', 'rapJitter',
             'localShimmer']
    
    # higher order statistics
    fvalue += [pd.Series(seg).skew(), pd.Series(seg).kurt()]
    fidx += ['skewness', 'kurtosis']
    
    # wavelet subband energy
    # 0-125, 125-250, 250-500, 500-1000, 1000-2000, 2000-4000
    wc = pywt.wavedec(seg, 'haar', mode='symmetric', level=5)
    fvalue += [np.sum(i ** 2) for i in wc]
    fidx += ['wc_0hz - 62hz', 'wc_62hz - 125hz', 'wc_125hz - 250hz', 'wc_250hz - 500hz', 'wc_500hz - 1000hz', 'wc_1000hz - 2000hz']
    
    # spectral centroid 
    fvalue += list(np.mean(get_ssc(seg, sr), axis=0))
    fidx += get_fre_range(name='ssc')
    
    # subband energy 
    fvalue += list(np.mean(get_fbank_en(seg, sr), axis=0))
    fidx += get_fre_range(name='sb_en')
    
    # log subband energy 
    fvalue += list(np.mean(get_log_fbank_en(seg, sr), axis=0))
    fidx += get_fre_range(name='logsb_en')
    
    # MFCC
    fvalue += list(np.mean(get_mfcc(seg, sr), axis=0))
    fidx += ['cep_' + str(i) for i in range(24)]
    
    return fvalue, fidx
