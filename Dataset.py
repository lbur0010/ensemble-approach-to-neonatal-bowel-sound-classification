from python_speech_features import mfcc
from Features import get_features
from Util import blockPrint, enablePrint
from sklearn.utils import resample
from sklearn.preprocessing import StandardScaler
from feature_selector import FeatureSelector
from tqdm import tqdm
import pandas as pd
import numpy as np
import h5py as h5
import pickle
import librosa
import os
import sys

stdout = sys.stdout

class Dataset():
    def __init__(self, path: str, data_path: str, create_data: bool = True):
            self.labels = []
            self.mfcc_2d = []
            self.mfcc_1d = []
            self.subjects = []
            self.recLen = []
            self.features = []
            self.segLen = 6
            self.overlap = 0.1

            self.path = path
            self.data_path = data_path

            self.samples = self._get_samples(path)

            if create_data:
                self.create_dataset(path, data_path)
                self.normalize_features(self.features)
                self.calc_best_features(data_path)
            else:
                self.load_dataset(data_path)
                self.normalize_features(self.features)


    def create_dataset(self, path: str, output_path: str):
        for sample in tqdm(self.samples):
            annotation = self._read_annotation(f"{path}/{sample}.txt")
            wave, sr = librosa.load(f"{path}/{sample}.wav", sr=None)
            self.recLen.append(librosa.core.get_duration(y=wave, sr=sr))
            for lb in range(2):
                for values in annotation[lb]:
                    if values[1] - values[0] < self.segLen:
                        continue
                    else:
                        offset = 0
                        while True:
                            start = int((values[0]+offset)*sr)
                            end = int((values[0]+offset+self.segLen)*sr)
                            if values[0]+offset+self.segLen > values[1]:
                                break
                            seg = wave[start: end]
                            fvalue, self.fidx = get_features(seg, sr)
                            self.features.append(fvalue)
                            self.labels.append(lb)
                            self.subjects.append(sample)

                            tensor = mfcc(seg, sr, numcep=24)
                            tensor = np.reshape(tensor, (599, 24,)).astype('float32')
                            self.mfcc_2d.append(tensor)  # 2D MFCC features
                            self.mfcc_1d.append(np.mean(mfcc(seg, sr, numcep=24), axis=0)) # 1D MFCC features

                            offset += self.overlap

        data = pd.DataFrame(self.features, columns=self.fidx)
        data['subject'] = pd.Series(self.subjects, dtype=str)
        data['label'] = pd.Series(self.labels)
        data.to_csv(f"{output_path}/bowel sound features.csv")

        with open(f"{output_path}/fidx.pt", "wb") as fp:
            pickle.dump(self.fidx, fp)
        
        with h5.File(f"{output_path}/MFCC Dataset.h5", "w") as hf:
            hf.create_dataset("mfcc_data_2d", data=self.mfcc_2d)
            hf.create_dataset("mfcc_data_1d", data=self.mfcc_1d)

    def load_dataset(self, path: str):
        data = pd.read_csv(f"{path}/bowel sound features.csv")
        with open(f"{path}/fidx.pt", "rb") as fp:
            self.fidx = pickle.load(fp)

        with open(f"{path}/best_features.pt", "rb") as fp:
            self.best_features = pickle.load(fp)

        self.labels = data['label']
        self.subjects = data['subject'].apply(str)
        self.subjects.loc[self.subjects == '0'] = '0000000'
        self.features = data.iloc[:, 1:-2]

        with h5.File(f"{path}/MFCC Dataset.h5", "r") as hf:
            self.mfcc_2d = hf["mfcc_data_2d"][:]
            self.mfcc_1d = hf["mfcc_data_1d"][:]

    def normalize_features(self, features):
        features = np.array(self.features)
        self.normalized_features = StandardScaler().fit_transform(features)

    def calc_best_features(self, path):
        self.best_features = {}
        self.avg_best_features = {}
        for test_sub in tqdm(self.samples):
            train = []
            train_true = []
            train_lb = []
            test = []
            test_lb = []

            for idx, sub in enumerate(self.subjects):
                if sub == test_sub:
                    test.append(self.normalized_features[idx])
                    test_lb.append(self.labels[idx])
                else:
                    if self.labels[idx] == 1:
                        train_true.append(self.normalized_features[idx])
                    else:
                        train.append(self.normalized_features[idx])
                        train_lb.append(self.labels[idx])
            train += resample(train_true, replace=False, n_samples=len(train_lb))
            train_lb += [1 for i in train_lb]
            
            x_train = pd.DataFrame(train, columns=self.fidx)
            y_train = pd.DataFrame(train_lb)

            blockPrint()    # block print from feature selection
            fs = FeatureSelector(data=x_train, labels=y_train)
            fs.identify_all(selection_params={
                'missing_threshold': 0.6,
                'correlation_threshold': 0.98,
                'task': 'classification',
                'eval_metric': 'multi_logloss',
                'cumulative_importance': 0.99
            })
            train_removed_all_once = fs.remove(methods='all', keep_one_hot=True)
            enablePrint(stdout)   # enable print
            important_feature_list = fs.feature_importances[:20] 
            self.best_features[test_sub] = important_feature_list

            with open(f"{path}/best_features.pt", "wb") as fp:
                pickle.dump(self.best_features, fp)
        
        for test_sub in self.samples:
            for idx, f in enumerate(self.best_features[test_sub]['feature']):
                if f in self.avg_best_features.keys():
                    self.avg_best_features[f] += 1
                else:
                    self.avg_best_features[f] = 1
        
        self.ranked_features = sorted(self.avg_best_features.items(), key = lambda item: item[1], reverse=True)

    def _get_samples(self, path: str):
        samples = os.listdir(path)

        s = []

        for f in samples:
            k = f.split('.')
            print(str(k[0]) + '\n')
            s.append(k[0])

        samples = list(set(s))

        return samples

    def _read_annotation(self, file):
        df = pd.read_table(file, sep='\t', header=None)
        ann = {}
        segs_0 = []
        segs_1 = []

        # extract segments with peristalticSound
        for idx, label in enumerate(df[8]):
            if label.strip().find('peristalticSound') != -1:
                segs_1.append([df.loc[idx, 3], df.loc[idx, 5]])
            else:
                continue
        ann[1] = segs_1

        # extract segments without peristalticSound
        last = 0.00
        for segment in segs_1:
            if segment[0] - last > 3:
                segs_0.append([last, segment[0]])
            last = segment[1]
        ann[0] = segs_0

        return ann

    
