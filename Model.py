import tensorflow as tf
from keras.utils import np_utils
from keras.preprocessing import sequence
from keras.models import Sequential
from keras.layers import Dense, Embedding, Dropout
from keras.layers import LSTM
from keras.preprocessing.text import Tokenizer
from keras.preprocessing.sequence import pad_sequences
from keras.layers import Input, Flatten, Dropout, Activation,Reshape
from keras.layers import Conv1D, MaxPooling1D, AveragePooling1D, LSTM
from keras.models import Model
from keras.callbacks import ModelCheckpoint
from sklearn.preprocessing import LabelEncoder
from keras.layers import BatchNormalization, Lambda, Bidirectional
from keras.layers import Conv2D, MaxPooling2D,GlobalAveragePooling2D
from hmmlearn.hmm import MultinomialHMM
from keras.callbacks import EarlyStopping

class NeoNatalModel():
    def __init__(self, model_path:str, path_1d: str, path_2d: str, ensemble_path: str, lr: float = 1e-4, decay: float = 1e-7, optim: str = 'RMS'):
        self._create_1d_model()
        self._create_2d_model()       
        self.model_path = model_path 
        self.path_1d = path_1d
        self.path_2d = path_2d
        self.ensemble_path = ensemble_path
        self.lr = lr
        self.decay = decay
        self.optim = optim

    def create_ensemble(self, test_sub):
        input1 = Input(shape=(599, 24, 1))
        input2 = Input(shape=(24, 1))

        self.model_2d.load_weights(f"{self.model_path}/{self.path_2d}/{test_sub}.h5")
        self.model_2d.trainable = False

        self.model_1d.load_weights(f"{self.model_path}/{self.path_1d}/{test_sub}.h5")
        self.model_1d.trainable = False

        mfcc_2d_output = self.model_2d.layers[-4].output
        mfcc_1d_output = self.model_1d.layers[-4].output

        mfcc_2d_new = Model(inputs=self.model_2d.input, outputs=mfcc_2d_output)
        mfcc_1d_new = Model(inputs=self.model_1d.input, outputs=mfcc_1d_output)

        output1 = mfcc_2d_new(input1)
        output2 = mfcc_1d_new(input2)

        bilin = Lambda(self._outer_product, name='outer_product')([output1, tf.expand_dims(output2, axis=2)])

        x = Dense(16, activation='relu')(bilin)
        x = Dense(1, activation='sigmoid')(x)
        self.ensemble_model = Model(inputs=[input1, input2], outputs=x)
        if self.optim == "RMS":
            opt = tf.keras.optimizers.RMSprop(learning_rate=self.lr, decay=self.decay)
        elif self.optim == "Adam":
            opt = tf.keras.optimizers.Adam(learning_rate=self.lr, decay=self.decay)
        else:
            print("Specified optimizer does not exist.")
        self.ensemble_model.compile(loss='binary_crossentropy', optimizer=opt, metrics=['accuracy'])

    def load_ensemble(self, test_sub):
        self.create_ensemble(test_sub)
        self.ensemble_model.load_weights(f"{self.model_path}/{self.ensemble_path}/{test_sub}.h5")

    def path(self):
        return f"{self.model_path}/{self.ensemble_path}"

    def _create_1d_model(self):
        input_layer = tf.keras.layers.Input((24, 1))
        z = Conv1D(256, 8, padding='same')(input_layer)
        z = Activation('relu')(z)
        z = Conv1D(128, 4, padding='same')(z)
        z = Activation('relu')(z)
        z = Dropout(0.1)(z)
        z = MaxPooling1D(pool_size=(2))(z)
        z = Conv1D(128, 4, padding='same')(z)
        z = Activation('relu')(z)
        z = Conv1D(128, 8, padding='same')(z)
        z = Activation('relu')(z)

        output_layer = Flatten()(z)
        output_layer = Dense(16)(output_layer)
        output_layer = Dense(2, activation='softmax')(output_layer)

        self.model_1d = tf.keras.models.Model(inputs=input_layer, outputs=output_layer)
        opt = tf.keras.optimizers.RMSprop(learning_rate=0.00001, decay=1e-6)
        self.model_1d.compile(loss='binary_crossentropy', optimizer=opt, metrics=['accuracy'])

    def _create_2d_model(self):
        input_layer = tf.keras.layers.Input((599, 24, 1))
        z = Conv2D(128, 16, padding='same')(input_layer)
        z = Activation('relu')(z)
        z = MaxPooling2D((4, 2))(z)
        z = Conv2D(256, 8, padding='same')(z)
        z = Activation('relu')(z)
        z = Dropout(0.1)(z)
        z = MaxPooling2D((3, 2))(z)
        z = Conv2D(512, 4, padding='same')(z)
        z = Activation('relu')(z)
        z = MaxPooling2D((2, 2))(z)
        z = Conv2D(512, 4, padding='same')(z)
        z = Activation('relu')(z)
        z = MaxPooling2D((2, 2))(z)

        output_layer = tf.keras.layers.GlobalAveragePooling2D()(z)
        output_layer = Dense(16, activation='relu')(output_layer)
        output_layer = Dense(1, activation='sigmoid')(output_layer)

        self.model_2d = tf.keras.models.Model(inputs=input_layer, outputs=output_layer)
        opt = tf.keras.optimizers.Adam(learning_rate=1e-5, decay=1e-6)
        self.model_2d.compile(loss='binary_crossentropy', optimizer=opt, metrics="accuracy")

    def _outer_product(self, x):
        #Einstein Notation  [batch,1,1,depth] x [batch,1,1,depth] -> [batch,depth,depth]
        phi_I = tf.einsum('ijkm,ijkn->imn',x[0],x[1])
        
        # Reshape from [batch_size,depth,depth] to [batch_size, depth*depth]
        phi_I = tf.reshape(phi_I,[-1,x[0].shape[3]*x[1].shape[3]])
        
        # Divide by feature map size [sizexsize]
        size1 = int(x[1].shape[1])
        size2 = int(x[1].shape[2])
        phi_I = tf.divide(phi_I, size1*size2)
        
        # Take signed square root of phi_I
        y_ssqrt = tf.multiply(tf.sign(phi_I),tf.sqrt(tf.abs(phi_I)+1e-12))
        
        # Apply l2 normalization
        z_l2 = tf.nn.l2_normalize(y_ssqrt, axis=1)
        return z_l2

    