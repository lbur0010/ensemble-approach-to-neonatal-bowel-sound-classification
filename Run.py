from tqdm import tqdm
from Dataset import Dataset
from Model import NeoNatalModel
from Util import plot_history
from HSMM import *
from sklearn.utils import resample
from sklearn.metrics import confusion_matrix, accuracy_score, classification_report, roc_auc_score
from sklearn.preprocessing import StandardScaler
from sklearn.neural_network import MLPClassifier
from pathlib import Path
import tensorflow as tf
import pandas as pd
import numpy as np
import pickle
import os


class Run():
    def __init__(self, dataset: Dataset):
        self.dataset = dataset

    def train_test_ensemble(self, 
        model: NeoNatalModel, 
        num_epochs: int = 200, 
        batch_size: int = 8, 
        retrain: bool = False, 
        plot: bool = False, 
        plot_path: str = None, 
        under_sample: bool = True,
        patience: int = 10,
    ):
        dataset = self.dataset

        self.groundTruth = []
        self.ensemblePredictions = []
        self.ensembleProbaPredictions = {}
        
        ensemble_result = np.array([[0, 0], [0, 0]])
        patient_accuracy = []

        for test_sub in tqdm(dataset.samples):
            train_mfcc_2d = []
            train_true_mfcc_2d = []
            train_mfcc_1d = []
            train_true_mfcc_1d = []
            train_lb = []

            test_mfcc_2d = []
            test_mfcc_1d = []
            test_lb = []

            for idx, sub in enumerate(dataset.subjects):
                if sub == test_sub:
                    test_mfcc_2d.append(dataset.mfcc_2d[idx])
                    test_mfcc_1d.append(dataset.mfcc_1d[idx])   
                    test_lb.append(dataset.labels[idx])
                else:
                    if under_sample:
                        if dataset.labels[idx] == 1:
                            train_true_mfcc_2d.append(dataset.mfcc_2d[idx])  
                            train_true_mfcc_1d.append(dataset.mfcc_1d[idx])
                        else:
                            train_mfcc_2d.append(dataset.mfcc_2d[idx])
                            train_mfcc_1d.append(dataset.mfcc_1d[idx])
                            train_lb.append(dataset.labels[idx])
                    else:
                        train_mfcc_2d.append(dataset.mfcc_2d[idx])
                        train_mfcc_1d.append(dataset.mfcc_1d[idx])
                        train_lb.append(dataset.labels[idx])
            if under_sample:
                train_mfcc_2d += resample(train_true_mfcc_2d, replace=False, n_samples=len(train_lb))  # Down sampling
                train_mfcc_1d += resample(train_true_mfcc_1d, replace=False, n_samples=len(train_lb))  # Down sampling
                train_lb += [1 for i in train_lb]
            
            x_train_mfcc = np.array(train_mfcc_2d)
            x_train_1d = np.array(train_mfcc_1d)

            x_test_mfcc = np.array(test_mfcc_2d)
            x_test_1d = np.array(test_mfcc_1d)
            
            y_train = np.array(train_lb)
            y_test = np.array(test_lb)

            x_traincnn_mfcc = np.expand_dims(x_train_mfcc, axis=3)
            x_testcnn_mfcc = np.expand_dims(x_test_mfcc, axis=3)

            y_train = np.reshape(y_train, (-1, 1))
            y_test = np.reshape(y_test, (-1, 1))

            file = []
            for filename in os.listdir(model.path()):
                file.append(Path(filename).stem)
            if test_sub not in file or retrain == True:
                model.create_ensemble(test_sub)
                callbacks = [
                    tf.keras.callbacks.ModelCheckpoint(
                        filepath = f"{model.path()}/{test_sub}.h5",
                        save_best_only=True,
                        monitor="val_accuracy",
                        mode="max",
                        verbose=1,
                    ),
                    tf.keras.callbacks.EarlyStopping(
                        monitor='val_accuracy', 
                        verbose=1,
                        patience=patience,
                        mode='max',
                        restore_best_weights=True)
                ]
                history = model.ensemble_model.fit([x_traincnn_mfcc, x_train_1d], y_train, epochs=num_epochs, batch_size=batch_size, callbacks=callbacks,
                                    verbose=1, validation_split=0.2)
                if plot:
                    plot_history(history, test_sub, plot_path)

                prediction = model.ensemble_model.predict([x_testcnn_mfcc, x_test_1d])
                self.ensembleProbaPredictions[test_sub] = prediction.copy()
                self.ensemblePredictions += list(np.round(prediction))
                self.groundTruth += list(y_test)
                cm = confusion_matrix(
                    y_test, np.round(prediction), labels=[0, 1]
                )
                
                ensemble_result += cm

                patient_accuracy.append(accuracy_score(list(y_test), list(np.round(prediction))))
            else:
                model.load_ensemble(test_sub)
                prediction = model.ensemble_model.predict([x_testcnn_mfcc, x_test_1d])
                self.ensembleProbaPredictions[test_sub] = prediction.copy()
                self.ensemblePredictions += list(np.round(prediction))
                self.groundTruth += list(y_test)
                cm = confusion_matrix(
                    y_test, np.round(prediction), labels=[0, 1]
                )
                ensemble_result += cm

                patient_accuracy.append(accuracy_score(list(y_test), list(np.round(prediction))))

        print('Final result:', ensemble_result)
        print('Acc: ', accuracy_score(self.groundTruth, self.ensemblePredictions))
        print(classification_report(self.groundTruth, self.ensemblePredictions))

        patient_accuracy = np.array(patient_accuracy)
        print(patient_accuracy)
        print("Accuracy STD: " + str(patient_accuracy.std()))
        print("Accuracy Variance: " + str(patient_accuracy.var()))

    def train_test_MLP(self, MLP_path: str, retrain=False, alpha: float = 0.1, max_iter: int = 500, under_sample: bool = True):
        dataset = self.dataset
        self.groundTruth = []
        self.MLPPredictions = []
        self.MLPProbaPredictions = {}
        
        MLP_result = np.array([[0, 0], [0, 0]])
        patient_accuracy = []

        for test_sub in tqdm(dataset.samples):
            train_hc = []
            train_true_hc = []
            train_lb = []

            test_hc = []
            test_lb = []

            for idx, sub in enumerate(dataset.subjects):
                if sub == test_sub:
                    test_hc.append(dataset.normalized_features[idx])
                    test_lb.append(dataset.labels[idx])
                else:
                    if under_sample:
                        if dataset.labels[idx] == 1:
                            train_true_hc.append(dataset.normalized_features[idx])
                        else:
                            train_hc.append(dataset.normalized_features[idx])
                            train_lb.append(dataset.labels[idx])
                    else:
                        train_hc.append(dataset.normalized_features[idx])
                        train_lb.append(dataset.labels[idx])
            if under_sample:
                train_hc += resample(train_true_hc, replace=False, n_samples=len(train_lb))  # Down sampling
                train_lb += [1 for i in train_lb]

            x_train_hc = pd.DataFrame(train_hc, columns=dataset.fidx)
            y_train = pd.DataFrame(train_lb)

            x_test_hc = pd.DataFrame(test_hc, columns=dataset.fidx)
            y_test = pd.DataFrame(test_lb)

            x_train_hc = np.array(x_train_hc[dataset.best_features[test_sub]['feature']])
            x_test_hc = np.array(x_test_hc[dataset.best_features[test_sub]['feature']])

            scaler = StandardScaler().fit(x_train_hc)
            x_train_hc = scaler.transform(x_train_hc)
            x_test_hc = scaler.transform(x_test_hc)

            y_train = np.array(y_train[0])
            y_test = np.array(y_test[0])

            y_train = np.reshape(y_train, (-1, 1))
            y_test = np.reshape(y_test, (-1, 1))

            file = []
            for filename in os.listdir(MLP_path):
                file.append(Path(filename).stem)
            if test_sub not in file or retrain == True:
                clf = MLPClassifier(alpha=alpha, max_iter=max_iter, random_state=42)
                clf.fit(x_train_hc, y_train)
                with open(f"{MLP_path}/{test_sub}.pt", 'wb') as fp:
                    pickle.dump(clf, fp)

                prediction = clf.predict_proba(x_test_hc)

                self.MLPProbaPredictions[test_sub] = prediction.copy()
                self.MLPPredictions += list(clf.classes_[prediction.argmax(axis=1)])
                self.groundTruth += list(y_test)

                cm = confusion_matrix(
                    y_test, clf.classes_[prediction.argmax(axis=1)], labels=[0, 1]
                )
                MLP_result += cm

                patient_accuracy.append(accuracy_score(list(y_test), list(clf.classes_[prediction.argmax(axis=1)])))
            else:
                with open(f"{MLP_path}/{test_sub}.pt", 'rb') as fp:
                    clf = pickle.load(fp)

                prediction = clf.predict_proba(x_test_hc)

                self.MLPProbaPredictions[test_sub] = prediction.copy()
                self.MLPPredictions += list(clf.classes_[prediction.argmax(axis=1)])
                self.groundTruth += list(y_test)

                cm = confusion_matrix(
                    y_test, clf.classes_[prediction.argmax(axis=1)], labels=[0, 1]
                )
                MLP_result += cm

                patient_accuracy.append(accuracy_score(list(y_test), list(clf.classes_[prediction.argmax(axis=1)])))

        print('Final result:', MLP_result)
        print('Acc: ', accuracy_score(self.groundTruth, self.MLPPredictions))
        print(classification_report(self.groundTruth, self.MLPPredictions))

        patient_accuracy = np.array(patient_accuracy)
        print(patient_accuracy)
        print("Accuracy STD: " + str(patient_accuracy.std()))
        print("Accuracy Variance: " + str(patient_accuracy.var()))
            

    def refine_classification(self, refine_type="ensemble", clf = None):
        if refine_type == "MLP":
            assert clf is not None # If refining an MLP prediction then the classifier must be passed in
            self.newPredBestS_MLP = {}
            probaPredictions = self.MLPProbaPredictions
        elif refine_type == "ensemble":
            self.newPredBestS_ensemble = {}
            probaPredictions = self.ensembleProbaPredictions
        else:
            print("Incorrect refine type")

        dataset = self.dataset

        overallPred = {}
        newOverallPred = {}
        for s in tqdm(np.linspace(0.1, 5.0, num=50)):
            overallPred[str(s)] = []
            newOverallPred[str(s)] = []
            if refine_type == "ensemble":
                self.newPredBestS_ensemble[str(s)] = {}
            elif refine_type == "MLP":
                self.newPredBestS_MLP[str(s)] = {}

            for test_sub in dataset.samples:
                prediction = probaPredictions[test_sub]

                if refine_type=="ensemble":
                    temp_observations = prediction.reshape(-1, )
                    overallPred[str(s)] += list(np.round(prediction))
                elif refine_type=="MLP":
                    if clf.classes_[0] == 0:
                        HSMM_observation = prediction[:, 1]
                    else:
                        HSMM_observation = prediction[:, 0]
                    temp_observations = HSMM_observation
                    overallPred[str(s)] += list(clf.classes_[prediction.argmax(axis=1)])

                sub_labels = pd.concat([pd.Series(dataset.subjects), pd.Series(dataset.labels)], axis=1)

                tm = np.array([
                    [0.001, 0.999],
                    [0.999, 0.001]
                ])

                means = np.array([0.0, 1.0])
                scales = np.array([s, s])

                dm = new_hsmm_get_duration(test_sub, sub_labels, dataset.samples)
                st = hsmm_get_init_probability(dataset.samples, test_sub, dataset.subjects, dataset.labels)

                # h1 = GaussianHSMM(means, scales, dm, tm, st)
                h1 = HSMMModel(LaplaceEmissions(means, scales), dm, tm, st)

                observations = np.zeros_like(temp_observations, dtype='float64')
                for idx, sta in enumerate(temp_observations):
                    observations[idx] = sta

                new_prediction = h1.decode(observations)

                newOverallPred[str(s)] += list(new_prediction)

                if refine_type == "ensemble":
                    self.newPredBestS_ensemble[str(s)][test_sub] = list(new_prediction)
                elif refine_type == "MLP":
                    self.newPredBestS_MLP[str(s)][test_sub] = list(new_prediction)

        x = []
        y1 = []
        y2 = []

        for s in np.linspace(0.1, 5.0, num=50):
            x.append(s)
            y1.append(accuracy_score(self.groundTruth, newOverallPred[str(s)]))
            y2.append(roc_auc_score(self.groundTruth, newOverallPred[str(s)]))
            print(s)
            print(roc_auc_score(self.groundTruth, newOverallPred[str(s)]))
            print(accuracy_score(self.groundTruth, newOverallPred[str(s)]))
            print()

        best_acc = max(y1)
        best_ind = y1.index(best_acc)

        self.s = x[best_ind]
        print('Best S:')
        print(self.s)
        print('Before Refinement: ')
        print(classification_report(self.groundTruth, overallPred[str(self.s)], digits=4))
        print('After Refinement: ')
        print(classification_report(self.groundTruth, newOverallPred[str(self.s)], digits=4))
        print()
        print('Before Refinement: ')
        print('ACC: ', accuracy_score(self.groundTruth, overallPred[str(self.s)]))
        print('After Refinement: ')
        print('ACC: ', accuracy_score(self.groundTruth, newOverallPred[str(self.s)]))
        print()
        print('Before Refinement: ')
        print('AUC: ', roc_auc_score(self.groundTruth, overallPred[str(self.s)]))
        print('After Refinement: ')
        print('AUC: ', roc_auc_score(self.groundTruth, newOverallPred[str(self.s)]))
        print('Confusion Matrix: ')
        print(confusion_matrix(self.groundTruth, newOverallPred[str(self.s)]))

        if refine_type == "ensemble":
            with open(f"{dataset.data_path}/ensemble_HSMM.pt", "wb") as fp:
                pickle.dump(self.newPredBestS_ensemble[str(self.s)], fp)
        elif refine_type == "MLP":
            with open(f"{dataset.data_path}/MLP_HSMM.pt", "wb") as fp:
                pickle.dump(self.newPredBestS_MLP[str(self.s)], fp)

    def load_refinement(self, refine_type="ensemble"):
        dataset = self.dataset
        if refine_type == "ensemble":
            with open(f"{dataset.data_path}/ensemble_HSMM.pt", "rb") as fp:
                self.newPredBestS_ensemble = pickle.load(fp)
        elif refine_type == "MLP":
            with open(f"{dataset.data_path}/MLP_HSMM.pt", "rb") as fp:
                self.newPredBestS_MLP = pickle.load(fp)

    def averaged_HSMM_refinement(self):
        assert self.newPredBestS_ensemble is not None and self.newPredBestS_MLP is not None # Both results must be refined

        dataset = self.dataset

        overallPred = {}
        newOverallPred = {}
        s = 5.0
        overallPred[str(s)] = []
        newOverallPred[str(s)] = []
        for test_sub in dataset.samples:
            prediction_1 = self.newPredBestS_ensemble[test_sub]
            prediction_2 = self.newPredBestS_MLP[test_sub]
            
            prediction = np.average([prediction_1, prediction_2], axis=0)
            overallPred[str(s)] += list(np.round(prediction))
            sub_labels = pd.concat([pd.Series(dataset.subjects), pd.Series(dataset.labels)], axis=1)

            tm = np.array([
                [0.001, 0.999],
                [0.999, 0.001]
            ])

            means = np.array([0.0, 1.0])
            scales = np.array([s, s])

            dm = new_hsmm_get_duration(test_sub, sub_labels, dataset.samples)
            st = hsmm_get_init_probability(dataset.samples, test_sub, dataset.subjects, dataset.labels)

            # h1 = GaussianHSMM(means, scales, dm, tm, st)
            h1 = HSMMModel(LaplaceEmissions(means, scales), dm, tm, st)

            temp_observations = prediction.reshape(-1, )
            observations = np.zeros_like(temp_observations, dtype='float64')
            for idx, sta in enumerate(temp_observations):
                observations[idx] = sta

            new_prediction = h1.decode(observations)

            newOverallPred[str(s)] += list(new_prediction)

        x = []
        y1 = []
        y2 = []

        x.append(s)
        y1.append(accuracy_score(self.groundTruth, newOverallPred[str(s)]))
        y2.append(roc_auc_score(self.groundTruth, newOverallPred[str(s)]))
        print(s)
        print(roc_auc_score(self.groundTruth, newOverallPred[str(s)]))
        print(accuracy_score(self.groundTruth, newOverallPred[str(s)]))
        print()

        print('Before Refinement: ')
        print(classification_report(self.groundTruth, overallPred[str(s)], digits=4))
        print('After Refinement: ')
        print(classification_report(self.groundTruth, newOverallPred[str(s)], digits=4))
        print()
        print('Before Refinement: ')
        print('ACC: ', accuracy_score(self.groundTruth, overallPred[str(s)]))
        print('After Refinement: ')
        print('ACC: ', accuracy_score(self.groundTruth, newOverallPred[str(s)]))
        print()
        print('Before Refinement: ')
        print('AUC: ', roc_auc_score(self.groundTruth, overallPred[str(s)]))
        print('After Refinement: ')
        print('AUC: ', roc_auc_score(self.groundTruth, newOverallPred[str(s)]))
        print('Before Refinement: ')
        print('Confusion Matrix: ')
        print(confusion_matrix(self.groundTruth, overallPred[str(s)]))
        print('After Refinement: ')
        print('Confusion Matrix: ')
        print(confusion_matrix(self.groundTruth, newOverallPred[str(s)]))

    def average_voting(self, clf):
        assert self.ensembleProbaPredictions is not None and self.MLPProbaPredictions is not None # Both results must be refined
        dataset = self.dataset

        overallPred = {}
        newOverallPred = {}
        for s in tqdm(np.linspace(0.1, 5.0, num=50)):
            overallPred[str(s)] = []
            newOverallPred[str(s)] = []
            for test_sub in dataset.samples:
                prediction_1 = self.ensembleProbaPredictions[test_sub]
                prediction_2 = self.MLPProbaPredictions[test_sub]
                if clf.classes_[0] == 0:
                    new_pred2 = prediction_2[:, 1]
                else:
                    new_pred2 = prediction_2[:, 0]
                prediction = np.average([prediction_1, new_pred2[:, None]], axis=0)
                overallPred[str(s)] += list(np.round(prediction))
                sub_labels = pd.concat([pd.Series(dataset.subjects), pd.Series(dataset.labels)], axis=1)

                tm = np.array([
                    [0.001, 0.999],
                    [0.999, 0.001]
                ])

                means = np.array([0.0, 1.0])
                scales = np.array([s, s])

                dm = new_hsmm_get_duration(test_sub, sub_labels, dataset.samples)
                st = hsmm_get_init_probability(dataset.samples, test_sub, dataset.subjects, dataset.labels)

                # h1 = GaussianHSMM(means, scales, dm, tm, st)
                h1 = HSMMModel(LaplaceEmissions(means, scales), dm, tm, st)

                temp_observations = prediction.reshape(-1, )
                observations = np.zeros_like(temp_observations, dtype='float64')
                for idx, sta in enumerate(temp_observations):
                    observations[idx] = sta

                new_prediction = h1.decode(observations)

                newOverallPred[str(s)] += list(new_prediction)

        x = []
        y1 = []
        y2 = []

        x.append(s)
        y1.append(accuracy_score(self.groundTruth, newOverallPred[str(s)]))
        y2.append(roc_auc_score(self.groundTruth, newOverallPred[str(s)]))
        print(s)
        print(roc_auc_score(self.groundTruth, newOverallPred[str(s)]))
        print(accuracy_score(self.groundTruth, newOverallPred[str(s)]))
        print()

        print('Before Refinement: ')
        print(classification_report(self.groundTruth, overallPred[str(s)], digits=4))
        print('After Refinement: ')
        print(classification_report(self.groundTruth, newOverallPred[str(s)], digits=4))
        print()
        print('Before Refinement: ')
        print('ACC: ', accuracy_score(self.groundTruth, overallPred[str(s)]))
        print('After Refinement: ')
        print('ACC: ', accuracy_score(self.groundTruth, newOverallPred[str(s)]))
        print()
        print('Before Refinement: ')
        print('AUC: ', roc_auc_score(self.groundTruth, overallPred[str(s)]))
        print('After Refinement: ')
        print('AUC: ', roc_auc_score(self.groundTruth, newOverallPred[str(s)]))
        print('Before Refinement: ')
        print('Confusion Matrix: ')
        print(confusion_matrix(self.groundTruth, overallPred[str(s)]))
        print('After Refinement: ')
        print('Confusion Matrix: ')
        print(confusion_matrix(self.groundTruth, newOverallPred[str(s)]))

    def max_voting(self, clf):
        assert self.ensembleProbaPredictions is not None and self.MLPProbaPredictions is not None # Both results must be refined
        dataset = self.dataset

        overallPred = {}
        newOverallPred = {}
        for s in tqdm(np.linspace(0.1, 5.0, num=50)):
            overallPred[str(s)] = []
            newOverallPred[str(s)] = []
            for test_sub in dataset.samples:
                prediction_1 = self.ensembleProbaPredictions[test_sub]
                prediction_2 = self.MLPProbaPredictions[test_sub]
                if clf.classes_[0] == 0:
                    new_pred2 = prediction_2[:, 1]
                else:
                    new_pred2 = prediction_2[:, 0]
                
                prediction = np.zeros_like(prediction_1)
                for i in range(prediction_1.shape[0]):
                    p1 = prediction_1[i].copy()
                    p2 = new_pred2[i].copy()
                    if np.abs(0.5 - p1) >= np.abs(0.5 - p2):
                        prediction[i] = p1
                    else:
                        prediction[i] = p2

                overallPred[str(s)] += list(np.round(prediction))
                sub_labels = pd.concat([pd.Series(dataset.subjects), pd.Series(dataset.labels)], axis=1)

                tm = np.array([
                    [0.001, 0.999],
                    [0.999, 0.001]
                ])

                means = np.array([0.0, 1.0])
                scales = np.array([s, s])

                dm = new_hsmm_get_duration(test_sub, sub_labels, dataset.samples)
                st = hsmm_get_init_probability(dataset.samples, test_sub, dataset.subjects, dataset.labels)

                # h1 = GaussianHSMM(means, scales, dm, tm, st)
                h1 = HSMMModel(LaplaceEmissions(means, scales), dm, tm, st)

                temp_observations = prediction.reshape(-1, )
                observations = np.zeros_like(temp_observations, dtype='float64')
                for idx, sta in enumerate(temp_observations):
                    observations[idx] = sta

                new_prediction = h1.decode(observations)

                newOverallPred[str(s)] += list(new_prediction)

        x = []
        y1 = []
        y2 = []

        x.append(s)
        y1.append(accuracy_score(self.groundTruth, newOverallPred[str(s)]))
        y2.append(roc_auc_score(self.groundTruth, newOverallPred[str(s)]))
        print(s)
        print(roc_auc_score(self.groundTruth, newOverallPred[str(s)]))
        print(accuracy_score(self.groundTruth, newOverallPred[str(s)]))
        print()

        print('Before Refinement: ')
        print(classification_report(self.groundTruth, overallPred[str(s)], digits=4))
        print('After Refinement: ')
        print(classification_report(self.groundTruth, newOverallPred[str(s)], digits=4))
        print()
        print('Before Refinement: ')
        print('ACC: ', accuracy_score(self.groundTruth, overallPred[str(s)]))
        print('After Refinement: ')
        print('ACC: ', accuracy_score(self.groundTruth, newOverallPred[str(s)]))
        print()
        print('Before Refinement: ')
        print('AUC: ', roc_auc_score(self.groundTruth, overallPred[str(s)]))
        print('After Refinement: ')
        print('AUC: ', roc_auc_score(self.groundTruth, newOverallPred[str(s)]))
        print('Before Refinement: ')
        print('Confusion Matrix: ')
        print(confusion_matrix(self.groundTruth, overallPred[str(s)]))
        print('After Refinement: ')
        print('Confusion Matrix: ')
        print(confusion_matrix(self.groundTruth, newOverallPred[str(s)]))